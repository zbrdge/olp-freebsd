RUST_SRCS!=find src -name '*.rs'
SRCS=device_if.h bus_if.h pci_if.h stubs.c
KMOD=olympic
CLEANDIRS+=target/
RUST_OBJ=target/debug/olympic.o
OBJS+=${RUST_OBJ}
.PATH: ${.CURDIR}/@/libkern
LIBCLANG_PATH?=/usr/local/llvm34/lib

${RUST_OBJ}: ${RUST_SRCS} Cargo.toml
	LIBCLANG_PATH=${LIBCLANG_PATH} cargo rustc -v -- --emit=obj -C relocation-model=static -C code-model=kernel

.include <bsd.kmod.mk>
