/*
 * Copyright (c) 2014 William Orr <will@worrbase.com>. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this list
 * of conditions and the following disclaimer.  Redistributions in binary form must
 * reproduce the above copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided with the
 * distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <sys/param.h>
#include <sys/kernel.h>
#include <sys/module.h>
#include <sys/socket.h>

#include <net/if.h>
#include <net/if_var.h>
#include <net/if_media.h>

#include <sys/bus.h>
#include <sys/libkern.h>

#include <dev/pci/pcivar.h>

#include "olympic.h"

#define OLYMPIC_DEBUG 1

char __morestack[1024];
char _GLOBAL_OFFSET_TABLE_;

int my_pci_enable_busmaster(device_t);

extern int rust_olp_probe(device_t);
extern int rust_olp_attach(device_t);
extern int rust_olp_detach(device_t);
extern int rust_olp_shutdown(device_t);

static int olp_probe(device_t);
static int olp_attach(device_t);
static int olp_detach(device_t);
static int olp_shutdown(device_t);

static device_method_t olp_methods[] = {
	DEVMETHOD(device_probe, olp_probe),
	DEVMETHOD(device_attach, olp_attach),
	DEVMETHOD(device_detach, olp_detach),
	DEVMETHOD(device_shutdown, olp_shutdown),
	{ 0, 0 },
};

static driver_t olp_driver = {
	"olp",
	olp_methods,
	sizeof(struct olp_softc),
};

static devclass_t olp_devclass;

DRIVER_MODULE(olp, pci, olp_driver, olp_devclass, NULL, NULL);
MODULE_DEPEND(olp, pci, 1, 1, 1);
MODULE_DEPEND(olp, iso88025, 1, 1, 1);

static int
olp_probe(device_t dev)
{

	return rust_olp_probe(dev);
}

static int
olp_attach(device_t dev)
{

	return rust_olp_attach(dev);
}

static int
olp_detach(device_t dev)
{

	return rust_olp_detach(dev);
}

static int
olp_shutdown(device_t dev)
{
	return rust_olp_shutdown(dev);
}

int
my_pci_enable_busmaster(device_t dev)
{

	return pci_enable_busmaster(dev);
}

