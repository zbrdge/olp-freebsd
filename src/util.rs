use core::str::StrExt;
use gen::printf;

pub struct CString {
    len: usize,
    data: *const [u8],
}

impl CString {
    pub fn to_cstr(&self) -> *const i8 {
        self.data as *const i8
    }

    pub fn new(s: &str) -> CString {
        CString { data: s.as_bytes(), len: s.len() }
    }
}

pub fn print_str(s: &str) {
    let mesg = CString::new(s);
    unsafe {
        printf(mesg.to_cstr());
    }
}
